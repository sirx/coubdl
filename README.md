# coubdl
CLI Tool for downloading loops from coub.com, inspired by a commment from Kagami in the youtube-dl issue section: https://github.com/rg3/youtube-dl/issues/13754#issuecomment-336673153

Dependencies: youtube-dl, jq & ffmpeg, webm.py

`sudo pacman -S youtube-dl jq ffmpeg python`

Set up your python so you can use pip (if pip is not installed check how to install it for your distro) then do `pip install webm`

this will install webm.py on your system you can check by just typing webm into your shell.
