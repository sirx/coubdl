#!/usr/bin/env bash
set -e
id="$(youtube-dl -sj "$1" | jq -r '.id')"
title="$(youtube-dl -sj "$1" | jq -r '.fulltitle')"
duration=($(youtube-dl -sj "$1" | jq -r '.duration'))
songduration="$(curl https://coub.com/api/v2/coubs/$id/audio_track | jq -r '.[].file')"
urlduration=($(ffprobe -show_streams -print_format json "$songduration" -v fatal | jq -r '.streams[0].duration'))
#songduration="$(curl https://coub.com/api/v2/coubs/$id/audio_track | jq '.[].file_m4a.meta.duration')"
baseurl=($(youtube-dl -sj "$1" | jq -r '.webpage_url'))
echo "$title" with a video duration of "$duration" seconds and a song duration of "$urlduration" seconds, please choose how long your video should be!
read vidduration
echo your video will be "$vidduration" seconds long!
youtube-dl -o 1.mp4 "$1"
youtube-dl -f html5-audio-high -o 1.mp3 "$1"
printf '\x00\x00' | dd of=1.mp4 bs=1 count=2 conv=notrunc
for i in `seq 1 100`; do echo "file '1.mp4'" >> 1.txt; done
ffmpeg -hide_banner -f concat -i 1.txt -i 1.mp3 -c copy -shortest -movflags faststart -t "$vidduration" coubed.mp4
rm 1.mp4 1.mp3 1.txt
echo Please enter your desired output size!
read size
echo you video will be "$vidduration" seconds long and "$size" MB big
music="$(curl https://coub.com/api/v2/coubs/$id/audio_track | jq -r '.[] | .artist, .title' | awk 'NR > 1 { printf(" - ") } {printf "%s",$0}')"
webm -l "$size" -vp8 -vorbis -mt "Song: $music" -i coubed.mp4 "$title".webm
#ffmpeg -i coubed.mp4 -threads 4 -c:v libvpx -speed 1 -lag-in-frames 25 -g 128 -pix_fmt yuv420p -b:v 2M "$title".webm
rm coubed.mp4
